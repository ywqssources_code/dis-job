package com.xxl.job.admin;


import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author xuxueli 2018-10-28 00:38:13
 */
@SpringBootApplication(exclude = {MybatisAutoConfiguration.class, DataSourceAutoConfiguration.class})
public class XxlJobAdminApplication {

	private static final Logger log = LoggerFactory.getLogger(XxlJobAdminApplication.class);

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		log.info("开始启动分布式定时任务");
        SpringApplication.run(XxlJobAdminApplication.class, args);
        long end = System.currentTimeMillis();
        log.info("启动分布式定时任务成功: [{}]",end-start);
	}

}