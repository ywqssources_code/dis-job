package com.xxl.job.admin.config;

import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * mysql数据库自动装配配置文件
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-07-03 17:53
 * @since 1.0.0
 **/
@ConfigurationProperties(prefix=HikariDataSourceProperties.HIKARI_DATASOURCE_PREFIX)
public class HikariDataSourceProperties {

    public static final String HIKARI_DATASOURCE_PREFIX = "mysql.datasource";

    private String jdbcUrl;

    private String username;

    private String password;

    private String driverClassName;

    private int maximumPoolSize=10;

    private int idleTimeout=3000;

    private int maxLifetime=18000;

    private int connectionTimeout=5000;

    private String connectionTestQuery;

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    public int getIdleTimeout() {
        return idleTimeout;
    }

    public void setIdleTimeout(int idleTimeout) {
        this.idleTimeout = idleTimeout;
    }

    public int getMaxLifetime() {
        return maxLifetime;
    }

    public void setMaxLifetime(int maxLifetime) {
        this.maxLifetime = maxLifetime;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public String getConnectionTestQuery() {
        return connectionTestQuery;
    }

    public void setConnectionTestQuery(String connectionTestQuery) {
        this.connectionTestQuery = connectionTestQuery;
    }
}
